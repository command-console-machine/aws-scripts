#import time & physical computing libraries
import time
import RPi.GPIO as gpio

#Assign button to the 4th pin, as a pull-up
gpio.setmode(gpio.BCM)
gpio.setup(4, gpio.IN, gpio.PUD_UP)

input = gpio.input(4)

"""Monitor for button presses. Do a thing, once, when the button is pressed.
"""

#prevInput = 0

while True:
	input = gpio.input(4)
	if ((not prevInput) and input):
		print("Button's off")
		time.sleep(1)	  
		#prevInput = input
		print("prev input is now %s" % prevInput)
	else:
		print(input)
		time.sleep(1)
		#prevInput = input
		print(input == True)
		print("Button's on")
